@echo off
:: luis.miguelcv2@gmail.com
:: fiverr.com/luismiguel_23

:init
	set root=%1
	if not defined root set root=%cd%
	set file="%cd%\file.csv"
	
	call :main 
goto :eof

:main
	echo.
	echo Root: %root%
	pushd %root%
	FOR /F "delims=·" %%A IN ('dir /ad /b /s') do set folder=%%A&call :validate
	popd
goto :eof
 
:validate
	setlocal EnableExtensions EnableDelayedExpansion
	:: Delete root
	set "string=!folder:%cd%=!" 
	set "str=!string:\=!"

	call :strLen string len
	call :strLen str lenB
	set /A count = %len% - %lenB% 
	if %count% == 2 call :csv "%string:~1%"
	endlocal
goto:eof

:csv 
	:: Create csv file
	if not exist %file% (
		echo Artist, Album > %file%
	)
	
	set line=%1
	:: Adding line to file
	echo   Adding %line% to file...
	echo %line:\=","% >> %file%
goto :EOF
	
:strLen string len
	(SETLOCAL ENABLEDELAYEDEXPANSION
	 set "str=A!%~1!"
	 set "len=0"
	 for /L %%A in (12,-1,0) do (set /a "len|=1<<%%A"
		for %%B in (!len!) do if "!str:~%%B,1!"=="" set /a "len&=~1<<%%A")
	)
	ENDLOCAL&IF "%~2" NEQ "" SET /a %~2=%len%
goto :EOF