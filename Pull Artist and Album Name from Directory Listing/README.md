# Pull Artist and Album Name from Directory Listing

The Directory Listing includes Files and Directories from an Audio Library. I want to pull the following from it:
	•	Artist Name
	•	Album Name

The result should be a delimited text file. Either a CSV file where the Artist and Album Names may need “” due to embedded commas or a different delimiter like ; can be used.

A header row is required “Artist”,”Album”.

Examples:

This line will be ignored as it is not a folder containing an Album
D:\Audiolib2\Zero 7

This line will be processed into Artist & Album

D:\Audiolib2\1975, The\The 1975

Generates a line in the output file

“1975, The”,”The 1975”

This line will be ignored as it is a Track/File rather than a folder.

D:\Audiolib2\1975, The\The 1975\06 1975, The - Talk!.flac
