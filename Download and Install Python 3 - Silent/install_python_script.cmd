@echo off
:: luis.miguelcv2@gmail.com
:: fiverr.com/luismiguel_23
:: To install another version of Python 3, change the URL in the urlPython variable 
:: and the name of the executable in the localPython variable

:init
	set urlPython=https://www.python.org/ftp/python/3.7.9/python-3.7.9-amd64.exe
	set localPython=python-3.7.9-amd64.exe
	set nameExe=Python3
	for /f "tokens=*" %%f in ('python --version^|findstr /c:"Python 3"') do ( goto :msgIntalled )
	if not exist %localPython% call :download %urlPython% %localPython% %nameExe%
	call :main
goto :EOF

:main
	echo Installing %nameExe% ...
	%localPython% /quiet InstallAllUsers=1 PrependPath=1 Include_test=0
goto :EOF

:download
	echo Downloading %3 ...
	powershell -command Invoke-WebRequest -Uri %1 -outfile %2
goto :EOF

:msgIntalled
	echo Python 3 is already installed
	ping localhost -n 2 > nul
goto :EOF
