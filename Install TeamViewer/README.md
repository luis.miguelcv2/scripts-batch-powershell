# Install TeamViewer Custom Host Client

Check that TeamViewer is not already installed.
1. If it is already installed, uninstall it.
2. If it is not installed, proceed to the next step