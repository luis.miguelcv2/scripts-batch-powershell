@echo off
:: call line: script.cmd [MyConfigID] [MyTokenId] [MyStategy]
:: You need to rename the variables Msix86 and Msix64 to the actual name of the msi installer
:: luis.miguelcv2@gmail.com
:: fiverr.com/luismiguel_23

setlocal ENABLEDELAYEDEXPANSION

set MyConfigID=%1
set MyTokenId=%2
set MyStategy=%3
set SoftwareName=TeamViewer
set x86GUID=HKLM\SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall
set x64GUID=HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall
:: rename the variables Msix86 and Msix64 to the actual name of the msi installer
set Msix86=HOST_86.msi  
set Msix64=HOST_64.msi

:main
	call :search %x86GUID%
	call :search %x64GUID%

	if defined nameApp (
		call :uninstallApp %dirUninstall%
	)

	set MSI_INSTALL=%Msix64%
	if "%PROCESSOR_ARCHITECTURE%" == "x86" (
		set MSI_INSTALL=%Msix86%
	)
	
	call :installTV %MSI_INSTALL%
goto :eof



:search
	for /f "delims=" %%P in ('reg query "%1" /s /f "%SoftwareName%" 2^>nul ^| findstr "Uninstall\\%SoftwareName% Uninstall\\{"') do (
	  for /f "tokens=2*" %%A in ('reg query "%%P" /v "DisplayName" 2^>nul ^|findstr "DisplayName"') do (set nameApp=%%B)
	  for /f "tokens=2*" %%A in ('reg query "%%P" /v "UninstallString" 2^>nul ^|findstr "UninstallString"') do (set dirUninstall=%%B) 
	  for /f "tokens=2*" %%A in ('reg query "%%P" /v "DisplayVersion" 2^>nul ^|findstr "DisplayVersion"') do (set versionApp=%%B) 
	)
goto :eof

:uninstallApp
	echo Uninstaling TeamViewer
	msiexec /x %1
:wait
	tasklist /nh | FIND /I "msiexec.exe" >NUL
	if not errorlevel 1 ping localhost -n 3 >nul & goto wait
goto :eof

:installTV 
	if exist %1 (
		if defined MyTokenId (
			if defined MyConfigID (
				if defined MyStategy (
					if exist "%MyStategy%" (
						msiexec.exe /i %1 /qn CUSTOMCONFIGID=%MyConfigID% APITOKEN=%MyTokenId% ENABLEOUTLOOKPLUGIN="false" DESKTOPSHORTCUTS="1" SETTINGSFILE=%MyStategy%
					) else (
						echo.
						echo The configuration file %MyStategy% does not exist. Installation is aborted.
						pause
					)
				) else (
					msiexec.exe /i %1 /qn CUSTOMCONFIGID=%MyConfigID% APITOKEN=%MyTokenId% ENABLEOUTLOOKPLUGIN="false" DESKTOPSHORTCUTS="1"
				)
			else (
				echo. 
				echo CONFIG_ID parameter are missing. Installation is aborted.
			)
		) else (
			echo. 
			echo TOKEN_ID parameter are missing. Installation is aborted.
		)
	) else (
		echo.
		echo Host MSI: %1, not exist. Installation is aborted.
		pause
	)
goto :eof

endlocal



