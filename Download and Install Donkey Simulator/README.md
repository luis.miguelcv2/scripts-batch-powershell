# Download and Install Donkey Simulator

1. Download and unzip the simulator for your host pc platform from Donkey Gym Release.
2. Place the simulator where you like. For this example it will be ~/projects/DonkeySimLinux. Your dir will have a different name depending on 	platform.
3. Complete all the steps to install Donkey on your host pc: https://docs.donkeycar.com/guide/host_pc/setup_windows/
4. Setup DonkeyGym: cd ~/projects
	git clone https://github.com/autopowerracing/APRDonkeyCar
	cd gym-donkeycar
	conda activate donkey
	pip install -e .[gym-donkeycar]

Reference: https://docs.donkeycar.com/guide/simulator/