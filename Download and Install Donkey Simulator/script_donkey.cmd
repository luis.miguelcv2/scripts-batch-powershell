@echo off
:: luis.miguelcv2@gmail.com
:: fiverr.com/luismiguel_23

:init
    set pathSim="%cd%\simulator\"
	set nameSim="%cd%\simulator\DonkeySimWin.zip"
	set zipSim=DonkeySimWin.zip
	set uriSim=https://github.com/tawnkramer/gym-donkeycar/releases/download/v20.10.28/DonkeySimWin.zip
	set uriMconda=https://repo.anaconda.com/miniconda/Miniconda3-latest-Windows-x86_64.exe 
	set nameMconda=Miniconda3-latest-Windows-x86_64.exe
	set uriGit=https://github.com/git-for-windows/git/releases/download/v2.28.0.windows.1/Git-2.28.0-64-bit.exe
	set nameGit=Git-2.28.0-64-bit.exe
	call :main
	pause
goto :EOF

:main
	cls
	if not exist %pathSim% mkdir %pathSim%
	if not exist %nameSim% call :download %uriSim% %nameSim% SimulatorBinary
	
	:: Unzip Simulator binary
	pushd %pathSim%
		echo Unzip %zipSim%...
		powershell -command Expand-Archive -Force -Path %zipSim%
	popd
	
	if not exist %nameMconda% call :download %uriMconda% %nameMconda% Miniconda
	echo Installing Miniconda ...
	start /wait "" Miniconda3-latest-Windows-x86_64.exe /InstallationType=JustMe /RegisterPython=0 /S /D="%UserProfile%\Miniconda3"
	
	if not exist %nameGit% call :download %uriGit% %nameGit% Git
	echo Installing Git ...
	start /wait "" Git-2.28.0-64-bit.exe

	echo Cloning donkeycar ...
	git clone https://github.com/autopowerracing/donkeycar
	pushd donkeycar
	git checkout master
	
	set PATH=%PATH%;%userprofile%\miniconda3;%userprofile%\miniconda3\scripts;%userprofile%\miniconda3\Library\bin;
	conda env create -f install\envs\windows.yml
	conda init cmd.exe
	start cmd /c "conda activate donkey"
	pip install -e .[pc]
	pip install -e gym-donkeycar
	popd

	donkey createcar --path mycar
	cd mycar

goto :EOF

:download
	echo Downloading %3 ...
	powershell -command Invoke-WebRequest -Uri %1 -outfile %2
goto :EOF
