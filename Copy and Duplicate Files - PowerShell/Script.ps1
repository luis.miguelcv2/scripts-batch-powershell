# luis.miguelcv2@gmail.com
# fiverr.com/luismiguel_23

# Indicates the total number of times the files will be duplicated
$totalDuplicate = 10

# Variable that controls the success or failure of the process
$isSuccess = $false

# Current directory traversal
foreach( $folder in Get-ChildItem -Directory) { 
    $totalElements = @( Get-ChildItem $folder -File ).Count;
    $countName = 1;

    # All files are renamed to prevent a file from being duplicated
    $prefixCopy = Get-Random
    Get-ChildItem $folder -File | Rename-Item -NewName { $_.Name -replace $_.Name ,($prefixCopy.ToString()+$_.Name) }

    # It starts by randomly renaming each file with the sequence starting at 01
    foreach( $item in Get-ChildItem $folder -File | Get-Random -Count ($totalElements+1)) {
        $nameItem = $countName.ToString() + ($item.Extension);
        if($countName -lt 10) { $nameItem = "0"+$nameItem } 
        $countName++;
        
        if  (!(Test-Path "$folder\$nameItem")) {
            #Write-Host Rename $item to $nameItem
            Rename-item -Force "$folder\$item" -NewName $nameItem
            $isSuccess = $true;
        } 
    }
    $lastCount = $totalElements + 1

    # Starts the duplication process according to the variable $totalDuplicate
    for($i=1; $i -lt $totalDuplicate; $i++) {
        $initCount = $lastCount - $totalElements
        $endCount = $lastCount - 5
        if($initCount -lt 10) { $initCount = "0"+$initCount } 
        if($endCount -lt 10) { $endCount = "0"+$endCount }

        # Validates that the last 5 files from the previous step are not duplicated in the first round  
        if($totalElements -ge 10) {  
            # Next, all the files are randomly duplicated, not including the last 5.
            foreach($item in Get-ChildItem $folder -File | Where-Object { [int]$_.BaseName -ge ($initCount) -and [int]$_.BaseName -lt ($endCount) } | Get-Random -Count ($totalElements+1)) {
                $nameItem = $lastCount.ToString() + ($item.Extension);
                # Write-Host Copy File: $item to $nameItem 
                Copy-Item -Force "$folder\$item" -Destination "$folder\$nameItem"
                $lastCount++
            }

            # We proceed to copy the remaining 5
            foreach($item in Get-ChildItem $folder -File | Where-Object { [int]$_.BaseName -ge ([int]$endCount) -and [int]$_.BaseName -lt (([int]$endCount+5))} | Get-Random -Count 5) {
                $nameItem = $lastCount.ToString() + ($item.Extension);
                # Write-Host Copy File Last: $item to $nameItem 
                Copy-Item -Force "$folder\$item" -Destination "$folder\$nameItem"
                $lastCount++
            }
        } else { 
            # It is duplicated from the last round without taking into account the exception of the last 5 files
            foreach($item in Get-ChildItem $folder -File | Where-Object { [int]$_.BaseName -gt ([int]$initCount) }) {
                $nameItem = $lastCount.ToString() + ($item.Extension);
                if($lastCount -lt 10) { $nameItem = "0"+$nameItem } 
                # Write-Host Copy File: $item to $nameItem 
                Copy-Item -Force "$folder\$item" -Destination "$folder\$nameItem"
                $lastCount++
            }
        }
    } 
}

if (!($isSuccess)) {
    Write-Host "`nNo working folder was found within the current directory. Please try again.`n"
} else {
    Write-Host "`nProcess executed successfully!.`n"
}