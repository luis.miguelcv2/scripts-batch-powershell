1. the script renames the files in the working folder randomly according to the scheme 01.*, 02.*, 03.* ...

For example, if there are jpg files in the folder, it randomly renames the files 01.jpg, 02.jpg, 03.jpg etc. until all files are renamed.

It is also possible that there are different types of files, e.g. jpg and txt files, in the folder. In this case, e.g. 01.jpg, 02.txt, 03.txt, 04.jpg are created and when the script is restarted, a new random order such as 01.txt, 02.jpg, 03.jpg, 04.txt, ... is created.
The renaming function therefore ignores the file extensions and of course does not change the file extensions. All file types are used together as one file pool for renaming.

2. After renaming, the script duplicates all files in the working folder and renames these files again randomly, as in step 1. However, the script starts one number after the last number from before. So if in step 1 the last renamed file has the name 38.jpg, then in step 2 the random renaming of the duplicates starts with 39.*, e.g. 39.txt.

3. The script duplicates the files from step 2 (only the duplicates from step 2 are duplicated again, not the files from step 1). These files are also randomly renamed as in step 2, with the same rules. So if in step 2, for example, the last renamed file has the name 76.jpg, then in step 3 the random renaming of the duplicates starts with 77.*, so for example 77.txt.

I can set a number in the script how often the script should continue like this. For example, if I set 10, the script will make 10 such duplication rounds including renaming. If there are e.g. 20 files in the folder originally then the output is 200 files in this case at the end. Please mark within the script where I can enter this number (please create no command prompt).

There is one rule: The first five files of a newly renamed duplicate set must never contain one of the last five files of the previous renamed set. This means that if, for example, the last five files after a rename operation are 34.jpg, 35.txt, 36.jpg, 37.jpg, 38.txt, then in the next duplicate step the subsequent renames (39, 40, 41, 42, 43) must not be one of these five files. If this happens, then the script could, for example, automatically skip such files for the time being and later give a number at the end in the current duplicate set.

Exception: If there are only 2 to 10 files in the folder in the beginning and therefore the rule above does not work, the script makes an exception: Then, from step 2 onwards, the same random numbering sequence is used as in step 1. So in this case, all random file sequences are the same random ones per duplication round (not different like normal) and will be repeated again and again for all steps.
