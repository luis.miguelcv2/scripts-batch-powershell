# luis.miguelcv2@gmail.com
# fiverr.com/luismiguel_23

# Indicates the total number of times the files will be duplicated
$totalDuplicate = 5

# Variable that controls the success or failure of the process
$isSuccess = $false

# Process that is responsible for copying by blocks
function Process-Files {
    Param([int]$Init, [int]$Medium, [int]$LastCount, [String]$Folder) 
    $End = $Init + $Medium - 1
      
    # Randomly copy the first half
    foreach($item in Get-ChildItem $Folder -File | Where-Object { [int]$_.BaseName -ge [int]$Init -and [int]$_.BaseName -le ([int]$End) } | Get-Random -Count $Medium) {
        $nameItem = $LastCount.ToString() + ($item.Extension);
        if($LastCount -lt 10) { $nameItem = "0"+$nameItem } 
        Copy-Item -Force "$Folder\$item" -Destination "$Folder\$nameItem"
        $LastCount++
    }
    
    # Randomly copies the remaining second half.
    foreach($item in Get-ChildItem $Folder -File | Where-Object { [int]$_.BaseName -gt [int]$End -and [int]$_.BaseName -le [int]$totalElements} | Get-Random -Count $totalElements) {
        $nameItem = $LastCount.ToString() + ($item.Extension);
        if($LastCount -lt 10) { $nameItem = "0"+$nameItem } 
        Copy-Item -Force "$Folder\$item" -Destination "$Folder\$nameItem"
        $LastCount++
    }
    return $LastCount
}


# Current directory traversal
foreach( $folder in Get-ChildItem -Directory) { 
    $script:totalElements = @( Get-ChildItem $folder -File ).Count;
    $countName = 1;

    # All files are renamed to prevent a file from being duplicated
    $prefixCopy = Get-Random
    Get-ChildItem $folder -File | Rename-Item -NewName { $_.Name -replace $_.Name ,($prefixCopy.ToString()+$_.Name) }

    # It starts by randomly renaming each file with the sequence starting at 01
    foreach( $item in Get-ChildItem $folder -File | Get-Random -Count ($totalElements+1)) {
        $nameItem = $countName.ToString() + ($item.Extension);
        if($countName -lt 10) { $nameItem = "0"+$nameItem } 
        $countName++;
        
        if  (!(Test-Path "$folder\$nameItem")) {
            #Write-Host Rename $item to $nameItem
            Rename-item -Force "$folder\$item" -NewName $nameItem
            $isSuccess = $true;
        } 
    }
    $lastCount = $totalElements + 1

    $totalDivide = $totalElements;
    if(($totalDivide%2) -ne 0) { $totalDivide-- }
    
    $medium = [int]($totalDivide/2)

    $initCount = $lastCount - $totalElements
    if($initCount -lt 10) { $initCount = "0"+$initCount }

    # Starts the duplication process according to the variable $totalDuplicate
    for($i=1; $i -lt $totalDuplicate; $i++) {
        $lastCount = Process-Files -Init $initCount -Medium $medium -LastCount $lastCount -Folder $folder
    }
}

if (!($isSuccess)) {
    Write-Host "`nNo working folder was found within the current directory. Please try again.`n"
} else {
    Write-Host "`nProcess executed successfully!.`n"
}

