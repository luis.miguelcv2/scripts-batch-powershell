# luis.miguelcv2@gmail.com
# fiverr.com/luismiguel_23

$nameKB = "kB5010793"
$descriptKB = "2022-01 Cumulative Update for Windows 10 Version 21H1 for x64-based Systems (KB5010793)"
$pathTemp = $ENV:SystemDrive+"\Temp\"

# Verifies the existence of the previously downloaded update
function SearhKB  {
	Param([string]$KBName)
	Get-ChildItem -Path $pathTemp | Where-Object {$_.Name -like "*"+$KBName+"*.msu"} |
	ForEach-Object {
		$msuKB = $_.Name
	}
	return $msuKB;
}

# check if script is admin
$currentPrincipal = New-Object Security.Principal.WindowsPrincipal([Security.Principal.WindowsIdentity]::GetCurrent())
if( $currentPrincipal.IsInRole([Security.Principal.WindowsBuiltInRole]::Administrator) ) {

	Write-Host "`nProcess Started, this may take a few minutes..."

	$isInstalled = Get-Hotfix | Where-Object {$_.HotFixID -eq $nameKB}
	if (!$isInstalled) {		

		if(!(Test-Path $pathTemp)) {
			New-Item -ItemType Directory -Force -Path $pathTemp | Out-Null
		} else {
			$msuKB = SearhKB -KBName $nameKB
		}

		# Download automatic update
		if(!$msuKB) {
			try {
				# Install Module of MSCatalog
				if(-not (Get-Module MSCatalog)) {
					Write-Host "`nInstalling Nuget Package ..." -ForegroundColor green
					Install-PackageProvider -Name "NuGet" -Verbose -Force | Out-Null

					Write-Host "`nInstalling MSCatalog module ..." -ForegroundColor green
					Install-Module -Name "MSCatalog" -Force
				}

				Write-Host "`nDownloading $nameKB ..." -ForegroundColor green
				Get-MSCatalogUpdate -Search $descriptKB | Save-MSCatalogUpdate -Destination $pathTemp
			} catch {
				Write-Host "`nAn error occurred while downloading the update. The process is canceled.`n" -ForegroundColor red
			}
			$msuKB = SearhKB -KBName $nameKB
		} 
		
		if($msuKB) {
			$pkgKB = $pathTemp+$msuKB 
			Write-Host "`nInstalling Package $pkgKB" -ForegroundColor green
			# install package 
			Start-Process -FilePath "wusa.exe" -ArgumentList "$pkgKB /quiet /promptrestart" -Wait -PassThru | Out-Null

			Write-Host "`nVerifying installation..."
			$isInstalled = Get-Hotfix | Where-Object {$_.HotFixID -eq $nameKB}
			if($isInstalled) {
				Write-Host "`nSuccessful Installation!`n" -ForegroundColor green
			} else { 
				Write-Host "`nFailed Installation! Check if the update is compatible with your device`n"  -ForegroundColor red
			} 
		} else {
			Write-Host "`nThe update to install is not found. $pathTemp `n" -ForegroundColor red
		}
	} else {
		Write-Host "`nThe update is already installed.!`n" -ForegroundColor green
	}
} else {
    Write-Host "`nThis PowerShell Script must be run with Administrative Privileges or nothing will work.`n" -ForegroundColor red
}
