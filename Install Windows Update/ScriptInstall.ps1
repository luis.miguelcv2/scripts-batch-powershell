# luis.miguelcv2@gmail.com
# fiverr.com/luismiguel_23

$nameKB = "kB5010793"
$descriptKB = "2022-01 Cumulative Update for Windows 10 Version 21H1 for x64-based Systems (KB5010793)"
$pathTemp = $ENV:SystemDrive+"\Temp\"

# Verifies the existence of the previously downloaded update
function SearhKB  {
	Param([string]$KBName)
	Get-ChildItem -Path $pathTemp | Where-Object {$_.Name -like "*"+$KBName+"*.msu"} |
	ForEach-Object {
		$msuKB = $_.Name
	}
	return $msuKB;
}

# check if script is admin
$currentPrincipal = New-Object Security.Principal.WindowsPrincipal([Security.Principal.WindowsIdentity]::GetCurrent())
if( $currentPrincipal.IsInRole([Security.Principal.WindowsBuiltInRole]::Administrator) ) {

	Write-Host "Process Started, this may take a few minutes...`n"

	# Install Module of PSWindowsUpdate
	if(-not (Get-Module PSWindowsUpdate)) {
		Write-Host "Installing Nuget Package.."
		Install-PackageProvider -Name "NuGet" -Verbose -Force

		Write-Host "`nInstalling PSWindowsUpdate module.."
		Install-Module -Name "PSWindowsUpdate" -Force
	}

	$isInstalled = Get-WindowsUpdate -IsInstalled -KBArticleID $nameKB
	if (!$isInstalled) {
		# Install Module of MSCatalog
		if(-not (Get-Module MSCatalog)) {
			Write-Host "Installing MSCatalog module..."
			Install-Module -Name "MSCatalog" -Force
		}
		
		$msuKB = SearhKB -KBName $nameKB
		# Download automatic update
		if(!$msuKB) {
			try {
				Write-Host "Downloading $nameKB..."
				Get-MSCatalogUpdate -Search $descriptKB | Save-MSCatalogUpdate -Destination $pathTemp
			} catch {
				Write-Host "`nAn error occurred while downloading the update. The process is canceled.`n"
			}
			$msuKB = SearhKB -KBName $nameKB
		} 
		
		if($msuKB) {
			$pkgKB = $pathTemp+$msuKB 
			Write-Host "Installing Package $pkgKB ..."
			
			# install package 
			wusa.exe $pkgKB /quiet /norestart

			Write-Host "Verifying installation..."
			if(Get-WindowsUpdate -IsInstalled -KBArticleID $nameKB) {
				Write-Host "`nSuccessful Installation!`n"
			} else { 
				Write-Host "`nFailed Installation!`n" 
			} 
		} else {
			Write-Host "`nThe update to install is not found. $pathTemp `n"
		}
	} else {
		Write-Host "`nThe update is already installed.!`n"
	}
} else {
    Write-Host "`nThis PowerShell Script must be run with Administrative Privileges or nothing will work.`n"
}
