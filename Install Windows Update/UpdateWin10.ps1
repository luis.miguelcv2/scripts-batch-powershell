# luis.miguelcv2@gmail.com
# fiverr.com/luismiguel_23

$pathTemp = $ENV:SystemDrive+"\WindowsFU\Packages"
$url = 'https://go.microsoft.com/fwlink/?LinkID=799445'
$file = "$($pathTemp)\Win10Upgrade.exe"
$versionWin10 = "21H2"
$minimumSpace = 20
$wshell = New-Object -ComObject Wscript.Shell

# check if script is admin
$currentPrincipal = New-Object Security.Principal.WindowsPrincipal([Security.Principal.WindowsIdentity]::GetCurrent())
if( $currentPrincipal.IsInRole([Security.Principal.WindowsBuiltInRole]::Administrator)) {

  # installed version of windows
  $version = Get-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\Windows NT\CurrentVersion" | foreach {$_.DisplayVersion}

  if($version -eq $versionWin10) {
    Write-Host "`nVersion $versionWin10 is already installed!"  -ForegroundColor Cyan
  } else {
    if(!(Test-Path $pathTemp)) {
      New-Item -ItemType Directory -Force -Path $pathTemp | Out-Null
    }

    Write-Host "`nProcess Started, this may take a few minutes..."

    Write-Host "`nChecking available space ..." -ForegroundColor green
    $freespace = Get-WMIObject  -Class Win32_LogicalDisk | Where-Object {$_.DeviceID -eq $ENV:SystemDrive} | foreach {$_.freespace}
    $freespace = [math]::Round($freespace/ 1GB, 2)
  
    # Check a minimum space
    if($freespace -lt $minimumSpace) {
       Write-Host "`nThere is not enough free space to perform the update, a minimum of $minimumSpace GB is required!" -ForegroundColor Red
    } else {
      Write-Host "`nAvailable Space: $freespace GB" -ForegroundColor Cyan

      $webClient = New-Object System.Net.WebClient
      Write-Host "`nDownloading the latest update for Windows 10 ..." -ForegroundColor green
      $webClient.DownloadFile($url,$file)

      if((Test-Path $file)) {
        Write-Host "`nInstalling update ..." -ForegroundColor green
        Start-Process -FilePath $file -ArgumentList '/quietinstall /skipeula /auto upgrade /copylogs $dir' -Wait -PassThru
        Write-Host "`nProcess finished!`n" -ForegroundColor green

        $btn = $wshell.Popup("To proceed you need to restart the computer, do you agree?", 0, "Warning", 0x4 + 0x20);
      
        if([int]$btn -eq 6) {
          [reflection.assembly]::loadwithpartialname('System.Windows.Forms') | Out-Null
          [reflection.assembly]::loadwithpartialname('System.Drawing') | Out-Null
          $notify = new-object system.windows.forms.notifyicon
          $notify.icon = [System.Drawing.SystemIcons]::Information
          $notify.visible = $true
          $notify.showballoontip(10,'WARNING','The computer will restart!',[system.windows.forms.tooltipicon]::None)
          Start-Sleep -Seconds 2
          Restart-Computer -Force
        } else {
          Write-Host "The installation will complete when you restart the computer.`n" -ForegroundColor Cyan
        }
        
      } else {
        Write-Host "`nAn error occurred while downloading the update. The process is canceled.`n" -ForegroundColor red
      }
    }
  }
} else {
    Write-Host "`nThis PowerShell Script must be run with Administrative Privileges or nothing will work.`n" -ForegroundColor red
}
