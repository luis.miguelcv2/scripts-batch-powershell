@echo off
:: luis.miguelcv2@gmail.com
:: fiverr.com/luismiguel_23

:init
    setlocal ENABLEEXTENSIONS
    set pathLoad=c:\load.cmd
    call :main
    endlocal
goto :EOF

:main
    echo Ping 8.8.8.8 ...
    echo.
    for /f "tokens=*" %%f in ('ping -n 1 8.8.8.8 ^|findstr /c:"TTL"') do ( call :load "%%f" )
    echo Time Out. Waiting 20 secons ...
    ping -n 19 localhost > nul
    cls
    goto :main
goto :eof

:load
    echo Calling %pathLoad%
    call %pathLoad%
    exit
goto :eof