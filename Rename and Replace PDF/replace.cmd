@echo off
:: luis.miguelcv2@gmail.com
:: fiverr.com/luismiguel_23

:init
    setlocal ENABLEEXTENSIONS
		set pathFiles=c:\partage\dl
		call :main
    endlocal
goto :EOF

:main
    for /f "tokens=*" %%f in ('dir "%pathFiles%\*.pdf" /b /a-d /on ^|findstr /r "\([0-9]\).pdf"') do (call:replace "%%~nf" "%%~xf")
goto :eof

:replace
    set name=%~1
    set ext=%~2
    set short=%name:~,-4%
    move /y "%pathFiles%\%name%%ext%" "%pathFiles%\%short%%ext%"
goto :eof