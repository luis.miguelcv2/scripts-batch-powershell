@echo off
:: luis.miguelcv2@gmail.com
:: fiverr.com/luismiguel_23

:init
    setlocal ENABLEEXTENSIONS
		set uri=%1
		set output=%temp%\logwget.txt
		if [%uri%] == [] goto :error
		call :main
    endlocal
goto :EOF

:main
    ::wget %uri% --no-check-certificate directory-prefix=%_path% -o %output%
    wget %uri% --no-check-certificate -o %output%
    For /F "Delims=_" %%A In ('%windir%\system32\findstr /C:"200 OK" %output%') do ( goto :success )
    For /F "Delims=_" %%A In ('%windir%\system32\findstr /C:"unable to resolve host address" %output%') do ( goto :error )
    call :failed
goto :EOF

:error
    echo -1
goto :EOF

:failed
    echo 0
goto :EOF

:success
    echo 1
goto :EOF