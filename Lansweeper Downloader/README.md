# Lansweeper Downloader

1. Program must download and save a file from HTTP/HTTPS, and store in preset location.
2. Program must accept download path via variable being passed to it.
3. Program must provide status code on completion of execution, if success or fail of download
and save step.
