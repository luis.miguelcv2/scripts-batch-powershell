# Continuously Rename and Move files to a mapped network location

Call line:
RenameAndMove.bat <local folder> <network mapped folder>

If <network mapped folder> is not specified, then set default to "Z:\my_remote_folder\"
If <local folder> is not specified, then set default to "C:\Users\jerrw\Desktop\camera_images"

JPG files will appear into a folder on the local computer.
The JPG filenames will be in this format-
{BoothNumber}-{HHMMSS}-{ImageNumber}.JPG
for example A-163350-6702.JPG

Make sure the JPG file is not locked.  If it is, go to the next JPG file.

There will be a counter.txt file in <local folder> with a numerical value {GroupNumber}
Read the numerical value {GroupNumber}, pad the variable with leading zeros to obtain a minimum of 4 characters, and use it to rename the JPG to-
{BoothNumber}-{HHMMSS}-{GroupNumber}-{ImageNumber}.JPG
for example A-163350-0002-6702.JPG

Then move the JPG file to <network mapped folder>

Repeat this process every 3 seconds until Ctr-C is pressed.



# Create a counter.txt file with a numeral in there and increment it when key pressed


Call line:
RunCounter.bat <local folder> <starting numeral>

If <starting numeral> is not specified, then:
  If counter.txt exists inside <local folder>, increment 1 from the existing numeral inside counter.txt
  If counter.txt doesn't exist inside <local folder>, set <starting numeral> = 1

If <local folder> is not specified, then set default to "C:\Users\jerrw\Desktop\camera_images"

Write the <starting numeral> into counter.txt
Notify user of the starting value written into counter.txt

Loop:
Wait for user to do a key press
Increment the numeral by 1, and write the updated numeral into counter.txt
Notify user of the new value written into counter.txt
Goto Loop

Keeps in the loop until Ctrl-C is pressed
