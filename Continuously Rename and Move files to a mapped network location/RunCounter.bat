@echo off
:: script runcounter.bat
:: create a counter.txt file with a numeral in there and increment it when key pressed
:: call line: my_script.bat "<local folder>" <starting numeral>
:: luis.miguelcv2@gmail.com
:: fiverr.com/luismiguel_23

:init
	set lfolder=%1
	if not defined lfolder set lfolder=c:\users\jerrw\desktop\camera_images
	if not exist %lfolder% ( goto :errorfolder )
	set number=%2
	if not defined number (
		call :main
	) else (
		set /a next=%number% + 1
		call :fase3
	)
	break
goto :eof

:main
	if exist %lfolder%\counter.txt goto :fase2
	set /a number=1
	set /a next=1
	goto :fase3
	
	:fase2
		pushd %lfolder%
		for /f %%a in (counter.txt) do (
			set /a number=%%a
			set /a next=%%a + 1
		)			
		popd
		
	:fase3
		echo.
		echo starting value: %number%
		echo new value: %next%
		echo.
		echo.%next%>%lfolder%\counter.txt
		echo key pressed 
		pause>nul
		goto :fase2
goto :eof	

:errorfolder
	echo.
	echo folder not exist
	ping localhost -n 3 > nul
	break
goto :eof