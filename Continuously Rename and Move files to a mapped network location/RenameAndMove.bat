@echo off
:: script renameandmove.bat
:: continuously rename and move files to a mapped network location
:: call line: my_script.bat "<local folder>" "<network mapped folder>"
:: luis.miguelcv2@gmail.com
:: fiverr.com/luismiguel_23

:: set default folder
:init
	set lfolder=%1
	set rfolder=%2
	set error=0
	call :main c:\users\jerrw\desktop\camera_images z:\my_remote_folder\
	if %error% neq 1 (
		call :search
	)
goto :eof

:main
	echo %1 %2
	if not defined lfolder set lfolder=%1
	if not defined rfolder set rfolder=%2
	echo.
	echo remote folder: %rfolder%
	echo local  folder: %lfolder%
	echo.
	if not exist %lfolder% ( goto :errorfolder )
	if not exist %rfolder% ( goto :errorfolder )
goto :eof

:search
	echo.
	echo move file jpg
	pushd %lfolder%
	for %%i in (*-*-*.jpg) do (
		call :movefile "%%i"
	)
	popd
	echo wait 3 seconds ...
	ping localhost -n 3 >nul
	goto :search
goto :eof

:movefile
	set name=%~1
	call :getnumber
	for /f "tokens=1,2,3 delims=-" %%a in ("%name%") do (
	 echo old name: %name%
	 echo new name: %%a-%%b-%groupnumber%-%%c
	 move /y %name% %rfolder%\%%a-%%b-%groupnumber%-%%c
	 echo.
	)
goto :eof

:getnumber
	set /p number= <counter.txt
	:fase
		call :lenght %number%
		if %len% lss 4 (
			set number=0%number%
			goto :fase
		)
		set groupnumber=%number%
goto :eof

:lenght string
	setlocal enableextensions
	set "string=%~1"
	if "%tmpvar%"=="" set/a cont=0
	if not defined string endlocal & exit/b 1 
	call set "var=%%string%:~%cont%,1%%"
	if defined var (
		set "tmpvar=$" & set/a cont+=1 & call %~0 "%string%"
	) else (
		call set len=%%cont%%
	)
	endlocal & set "len=%len%"
exit /b

:errorfolder
	echo.
	echo folder not exist
	set error=1
	ping localhost -n 3 > nul
goto :eof