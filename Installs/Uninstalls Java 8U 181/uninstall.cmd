@echo off
:: Run with administrator permissions

:main
	set head32=26A24AE4-039D-4CA4-87B4-2F8321
	set head64=26A24AE4-039D-4CA4-87B4-2F8641
	set head32L=26A24AE4-039D-4CA4-87B4-2F321
	set head64L=26A24AE4-039D-4CA4-87B4-2F641
	set f=FF
	for /L %%i in (6,1,8) do (
		for /L %%j in (0,1,200) do (
			if %%j LSS 10 (
				call :uShort %%i00%%j %%i %%j
			) else ( 
				if %%j LSS 100 (
					call :uShort %%i0%%j %%i %%j
				) else (
					call :uLarge %%i0%%j %%i %%j
				)
			)
		)
	)
	goto :eof
	
:uShort
	echo.
	set var=%2
	set str=%1
	if "%var%" EQU "8" set f=F0
	set f32={%head32%%str%%f%}
	set f64={%head64%%str%%f%}
	call :uninstall %2 %3
	goto :eof

:uLarge
	echo.
	set var=%2
	set str=%1
	if "%var%" EQU "8" set f=F0
	set f32={%head32L%%str%%f%}
	set f64={%head64L%%str%%f%}
	call :uninstall %2 %3
	goto :eof
	
:uninstall
	:: Uninstall version Java 32B
	echo Searching and Uninstalling Java 32b version %1.%2 ...
	start /wait msiexec /x %f32% /qn /norestart
	cls
	:: Uninstall version Java 64B
	echo Searching and Uninstalling Java 64b version %1.%2 ...
	cls
	start /wait msiexec /x %f64% /qn /norestart
	goto :eof