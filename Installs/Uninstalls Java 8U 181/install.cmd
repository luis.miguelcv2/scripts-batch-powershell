@echo off
:: Run with administrator permissions
set appJ=jre-8u181-windows-i586.exe
:install
	:: Install Java 
	echo.
	echo Installing Java ...
	%appJ% /s INSTALL_SILENT=Enable
	::start /wait %appJ% /s INSTALL_SILENT=Enable
	call :updateOff
	goto :eof
	
:updateOff	
	reg delete "HKLM\SOFTWARE\Wow6432Node\JavaSoft\Java Update\Policy\jucheck" /v "UpdateSchedule" /f
	reg delete "HKLM\SOFTWARE\Wow6432Node\JavaSoft\Java Update\Policy\jucheck" /v "UpdateMin" /f
	:: Disable java updates
	reg add "HKLM\SOFTWARE\Wow6432Node\JavaSoft\Java Update\Policy" /v EnableJavaUpdate /t REG_DWORD /d 00000000 /f
	:: Disable Auto Update Check
	reg add "HKLM\SOFTWARE\Wow6432Node\JavaSoft\Java Update\Policy" /v EnableAutoUpdateCheck /t REG_DWORD /d 00000000 /f
	:: Disable Java Update
	reg add "HKCU\SOFTWARE\JavaSoft\Java Update\Policy" /v EnableJavaUpdate /t REG_DWORD /d 00000000 /f
	goto :eof