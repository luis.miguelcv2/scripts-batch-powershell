@echo off
:: luis.miguelcv2@gmail.com
:: fiverr.com/luismiguel_23

setlocal ENABLEDELAYEDEXPANSION

set SoftwareName=OfficeReports
set uriApp=C:\Temp\setup.exe
set SoftwareCode=35e91f2cddc49699
set HKU=HKEY_USERS\S-1-5-21-2833509057-2033408295-3559467026-1001\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall

:main
	call :copy %0
	call :search %HKU%
	if defined nameApp (
		echo %nameApp% Installed. Installation is aborted.
	) else (
		if exist %uriApp% (
			msg ** Installing OfficeReports
			%uriApp%
		) else (
			msg ** %uriApp% are missing. Installation is aborted.
		)
	)
goto :eof

:search
	for /f "delims=" %%P in ('reg query "%1" /s /f "%SoftwareCode%" 2^>nul ^| findstr "Uninstall\\%SoftwareCode% Uninstall\\{"') do (
	  for /f "tokens=2*" %%A in ('reg query "%%P" /v "DisplayName" 2^>nul ^|findstr "DisplayName"') do (set nameApp=%%B)
	  for /f "tokens=2*" %%A in ('reg query "%%P" /v "UninstallString" 2^>nul ^|findstr "UninstallString"') do (set dirUninstall=%%B) 
	  for /f "tokens=2*" %%A in ('reg query "%%P" /v "DisplayVersion" 2^>nul ^|findstr "DisplayVersion"') do (set versionApp=%%B) 
	)
goto :eof

:copy
	if not exist "%temp%\%~nx1" ( 
		copy %1 "%temp%\%~nx1" 
	)
	REG ADD HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Run /f /v Install%SoftwareName% /t REG_SZ /d "%temp%\%~nx1"
goto :eof

endlocal
